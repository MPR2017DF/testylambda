package pl.edu.pjwstk.mpr.lab5.service;

import pl.edu.pjwstk.mpr.lab5.domain.Permission;
import pl.edu.pjwstk.mpr.lab5.domain.Person;
import pl.edu.pjwstk.mpr.lab5.domain.Role;
import pl.edu.pjwstk.mpr.lab5.domain.User;


import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by Krzysztof Pawlowski on 22/11/15.
 */
public class UserService {

    public static List<User> findUsersWhoHaveMoreThanOneAddress(List<User> users) {
		return users.stream()
				.filter(user -> user.getPersonDetails().getAddresses().size() > 1)
				.collect(Collectors.toList());
    }

    public static Person findOldestPerson(List<User> users) {
    	return users.stream()
    			.map(user -> user.getPersonDetails())
    			.max(Comparator.comparing(person -> person.getAge()))
    			.orElse(null);
    }

    public static User findUserWithLongestUsername(List<User> users) {
    	return users.stream()
    			.max(Comparator.comparing(user -> user.getName().length()))
    			.orElse(null);     
    }

    public static String getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(List<User> users) {
        return users.stream()
        		.filter (user -> user.getPersonDetails().getAge()>18)
        		.map(user -> user.getPersonDetails().getName() + " " + user.getPersonDetails().getSurname())
        		.collect(Collectors.joining( ","));
      
    }

    public static List<String> getSortedPermissionsOfUsersWithNameStartingWithA(List<User> users) {
    	return users.stream()
    			.filter (user -> user.getPersonDetails().getName().startsWith("A"))
    			.flatMap(user -> user.getPersonDetails().getRole().getPermissions().stream())
    			.map(Permission::getName)
    			.sorted(String::compareTo)
    			.collect(Collectors.toList());
    }

    public static void printCapitalizedPermissionNamesOfUsersWithSurnameStartingWithS(List<User> users) {
    	users.stream()
    	.filter (user -> user.getPersonDetails().getSurname().startsWith("S"))
    	.flatMap(user -> user.getPersonDetails().getRole().getPermissions().stream())
    	.map(permission -> permission.getName().toUpperCase())
    	.forEach(System.out::println);
    	
        
    }

    public static Map<Role, List<User>> groupUsersByRole(List<User> users) {
    	return users.stream()
    	.collect(Collectors.groupingBy(user -> user.getPersonDetails().getRole()));     
    }

	public static Map<Boolean, List<User>> partitionUserByUnderAndOver18(List<User> users) {
    	return users.stream()
    			.collect(Collectors.partitioningBy(user -> user.getPersonDetails().getAge() > 18));
    }
    
}
