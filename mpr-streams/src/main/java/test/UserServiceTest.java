package test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import static java.util.Collections.emptyList;


import static org.assertj.core.api.Assertions.*;

import org.junit.Before;
import org.junit.Test;

import pl.edu.pjwstk.mpr.lab5.domain.Address;
import pl.edu.pjwstk.mpr.lab5.domain.Permission;
import pl.edu.pjwstk.mpr.lab5.domain.Person;
import pl.edu.pjwstk.mpr.lab5.domain.Role;
import pl.edu.pjwstk.mpr.lab5.domain.User;
import pl.edu.pjwstk.mpr.lab5.service.UserService;

public class UserServiceTest {

	private Role role1;
	private Role role2;
	private Role role3;
	private Role role4;

	Address address1;
	Address address2;

	Permission permission1;
	Permission permission2;
	Permission permission3;

	Person person1;
	Person person2;
	Person person3;

	User user1;
	User user2;
	User user3;

	List<User> users;

	@Before
	public void setupUsers() {

		permission1 = new Permission();
		permission1.setName("permissionA");
		permission2 = new Permission();
		permission2.setName("permissionB");
		permission3 = new Permission();
		permission3.setName("permissionC");

		role1 = new Role();
		role1.setPermissions(Arrays.asList(permission3));
		role2 = new Role();
		role2.setPermissions(Arrays.asList(permission1, permission1));
		role3 = new Role();
		role3.setPermissions(Arrays.asList(permission2));
		role4 = new Role();
		role4.setPermissions(Arrays.asList(permission2, permission3));

		address1 = new Address();
		address2 = new Address();

		person1 = new Person();
		person1.setName("Andrzej");
		person1.setAge(29);
		person1.setAddresses(Arrays.asList(address1));
		person1.setSurname("Nowak");
		person1.setRole(role4);

		person2 = new Person();
		person2.setName("Iwona");
		person2.setAge(15);
		person2.setAddresses(Arrays.asList(address2, address1));
		person2.setSurname("Sulin");
		person2.setRole(role3);

		person3 = new Person();
		person3.setName("Janusz");
		person3.setAge(21);
		person3.setAddresses(Arrays.asList(address2));
		person3.setSurname("Real");
		person3.setRole(role1);

		user1 = new User();
		user1.setName("u1");
		user1.setPersonDetails(person1);

		user2 = new User();
		user2.setName("uzytkownik2");
		user2.setPersonDetails(person2);

		user3 = new User();
		user3.setName("user3");
		user3.setPersonDetails(person3);

		users = Arrays.asList(user1, user2, user3);
	}

	@Test
	public void findUsersWhoHaveMoreThanOneAddress_UsersWithOneAddress_ListOfUsers() {
		assertThat(UserService.findUsersWhoHaveMoreThanOneAddress(users)).isNotEmpty().contains(user2)
				.doesNotContain(user1, user3);
	}

	@Test
	public void findUsersWhoHaveMoreThanOneAddress_EmptyList_EmptyList() {
		assertThat(UserService.findUsersWhoHaveMoreThanOneAddress(new ArrayList<User>())).isEmpty();
	}

	@Test
	public void findUsersWhoHaveMoreThanOneAddress_NullPointerException_Null() {
		assertThatExceptionOfType(NullPointerException.class)
				.isThrownBy(() -> UserService.findUsersWhoHaveMoreThanOneAddress(null));
	}

	@Test
	public void findUsersWhoHaveMoreThanOneAddress_EmptyList_UsersWithOneAddress() {
		assertThat(UserService.findUsersWhoHaveMoreThanOneAddress(Arrays.asList(user1, user3))).isEmpty();
	}

	@Test
	public void findOldestPerson_OldestPerson_ListOfUsers() {
		assertThat(UserService.findOldestPerson(users)).isEqualTo(person1);
	}

	@Test
	public void findOldestPerson_NullPointerException_Null() {
		assertThatExceptionOfType(NullPointerException.class).isThrownBy(() -> UserService.findOldestPerson(null));
	}

	@Test
	public void findOldestPerson_IsNull_EmptyList() {
		assertThat(UserService.findOldestPerson(new ArrayList<User>())).isNull();

	}

	@Test
	public void findUserWithLongestUsername_UserWithLongestUsername_ListOfUser() {
		assertThat(UserService.findUserWithLongestUsername(users)).isEqualTo(user2);
	}

	@Test
	public void findUserWithLongestUsername_NullPointerException_Null() {
		assertThatExceptionOfType(NullPointerException.class)
				.isThrownBy(() -> UserService.findUserWithLongestUsername(null));
	}

	@Test
	public void findUserWithLongestUsername_Null_EmptyList() {
		assertThat(UserService.findUserWithLongestUsername(new ArrayList<User>())).isNull();
	}

	@Test
	public void getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18_UsersNames_ListOfUsers() {
		assertThat(UserService.getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(users))
				.containsSequence(person1.getName() + " " + person1.getSurname() + ",")
				.containsSequence(person3.getName() + " " + person3.getSurname());
	}

	@Test
	public void getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18_NullPointerException_Null() {
		assertThatExceptionOfType(NullPointerException.class)
				.isThrownBy(() -> UserService.getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(null));
	}

	@Test
	public void getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18_EptyString_EmptyList() {
		assertThat(UserService.getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(new ArrayList<User>())).isEmpty();
	}
	@Test
	public void getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18_EmptyStrin_AllUsersUnder18() {
		assertThat(UserService.getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(Arrays.asList(user2))).isEmpty();
	}
	@Test
	public void getSortedPermissionsOfUsersWithNameStartingWithA_SortedList_ListOfUsers(){
		assertThat(UserService.getSortedPermissionsOfUsersWithNameStartingWithA(users))
		.isNotEmpty()
		.isSorted()
		.containsOnly(permission2.getName(),permission3.getName());
	}
	@Test
	public void getSortedPermissionsOfUsersWithNameStartingWithA_NullPointerException_Null(){
		assertThatExceptionOfType(NullPointerException.class)
		.isThrownBy(() -> UserService.getSortedPermissionsOfUsersWithNameStartingWithA(null));
	}
	@Test
	public void getSortedPermissionsOfUsersWithNameStartingWithA_EmptyStrin_EmptyList(){
		assertThat(UserService.getSortedPermissionsOfUsersWithNameStartingWithA(new ArrayList<User>())).isEmpty();
	}
	@Test
	public void groupUsersByRole_GroupUsers_ListOfUsers() {
		assertThat(UserService.groupUsersByRole(users))
		.contains(entry(role1, Arrays.asList(user3)))
		.contains(entry(role4, Arrays.asList(user1)))
		.contains(entry(role3, Arrays.asList(user2)));
	}
	@Test
	public void groupUsersByRole_NullPointerException_Null() {
		assertThatExceptionOfType(NullPointerException.class)
		.isThrownBy(() -> UserService.groupUsersByRole(null));	
	}
	@Test
	public void groupUsersByRole_EmptyMap_EmptyList() {
			assertThat(UserService.groupUsersByRole(new ArrayList<User>())).isEmpty();
	}
	@Test
	public void partitionUserByUnderAndOver18_GroupOfUsers_ListOfUsers(){
		assertThat(UserService.partitionUserByUnderAndOver18(users))
		.contains(entry(true, Arrays.asList(user1,user3)))
		.contains(entry(false, Arrays.asList(user2)));	
	}
	@Test
	public void partitionUserByUnderAndOver18_NullPointerException_Null() {
		assertThatExceptionOfType(NullPointerException.class)
		.isThrownBy(() -> UserService.partitionUserByUnderAndOver18(null));	
	}
	@Test
	public void partitionUserByUnderAndOver18_EmptyMap_EmptyList() {
			assertThat(UserService.partitionUserByUnderAndOver18(new ArrayList<User>()))
			.contains(entry(true, emptyList()))
			.contains(entry(false,emptyList()));	
	}
	
	
	

}
